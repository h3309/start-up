<?php

include 'config.php';

session_start();

if(count($_POST)>0) {
  mysqli_query($conn,"UPDATE stock set stock_id='" . $_POST['stock_id'] . "', product_name='" . $_POST['product_name'] . "', total_quantity='" . $_POST['total_quantity'] . "', price_per_unit='" . $_POST['price_per_unit'] .  "' WHERE stock_id='" . $_POST['stock_id'] . "'");
  $message = "Stock Updated Successfully!";
  }
  $result = mysqli_query($conn,"SELECT * FROM stock WHERE stock_id='" . $_GET['stock_id'] . "'");
  $row= mysqli_fetch_array($result);

  if(isset($message)){ 
    echo "<script>alert('$message');
          window.location.href='Dashboard_Analyst_latest.php';
    </script>";
  }


?>

<!doctype html>

<html lang="en">
  <head>
    <link rel="stylesheet" href="stock.css">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
   
  </head>
  <body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#">Start-Up Inventory Management</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link" href="Dashboard_Analyst_latest.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="login.html">Log Out</a>
            </li>
          </ul>
        </div>
      </nav>
      <div class="container mx-auto">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="Dashboard_Analyst_latest.php">Dashboard</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="stock-create.php">Stock Update</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Email</a>
                  </li>
            </ul>
        </nav>
      </div>
    

    <form action="" class ="form-group" method="post">
        <div class="form-group">
          <h1>Update Stock</h1>
          <p>Update directly in the box</p>
          <hr>

          <label for="stock_id"><b>Stock ID:</b></label>
          <input type="number" name="stock_id" id="stock_id" value="<?php echo $row['stock_id']; ?>">

          <hr>
      
          <label for="product_name"><b>Name of the Product:</b></label>
          <input type="text" name="product_name" id="product_name" value="<?php echo $row['product_name']; ?>" >
          <hr>
      
          <label for="total_quantity"><b>Quantity of product:</b></label>
          <input type="number" name="total_quantity"  id="total_quantity" value="<?php echo $row['total_quantity']; ?>" min=0>
          <hr>

          <label for="price_per_unit"><b>Price per unit:</b></label>
          <input type="number" step= "0.01"name="price_per_unit" id="price_per_unit" value="<?php echo $row['price_per_unit']; ?>" min=0>
          <hr>

          <div class="btn-submit">
            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </div>
      </form>


      <div class="container">
          <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
            <p class="col-md-4 mb-0 text-muted">&copy; Start Up Company, Inc</p>
        
            <a href="/" class="col-md-4 d-flex align-items-center justify-content-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
              <svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap"/></svg>
            </a>
          </footer>
        </div>

  </body>
</html>