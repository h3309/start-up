<?php
include_once 'config.php';
$result = mysqli_query($conn,"SELECT stock_id, product_name, total_quantity, price_per_unit, quantity_sold, date FROM stock");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Document</title>
</head>
<style>
    .navbar{
        margin-bottom: 70px;
    }
    .dropbtn {
  background-color: #000000;
  color: white;
  padding: 16px;
  padding-left: 20px;
  font-size: 16px;
  border: none;
}

.dropdown{
    float: right;
    position: relative;
}
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #ddd;}

.dropdown:hover .dropdown-content {display: block;}

.dropdown:hover .dropbtn {background-color: rgb(0, 153, 255);}
    ul.menu {
  list-style-type: none;
  margin-bottom: 50px;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

li.dash {
  float: left;
}

li.dash a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}
.download{
    margin-top: 20px;
    float: right;
    font-size: 20px;
    background-color: black;
    color: white;
    border-radius: 5px;
}
.download:hover{
    background-color: rgb(0, 153, 255);
}

#update{
  background-color: rgb(124, 105, 239);
  color: #f1f1f1;
  padding: 5px 10px;
  border-radius: 5px;
  border-color: black;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

#delete{
  background-color: red;
  color: #f1f1f1;
  padding: 5px 10px;
  border-radius: 5px;
  border-color: #f1f1f1;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

</style>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#">Start-Up Inventory Management</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link" href="Dashboard_Analyst_latest.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="login.html">Log Out</a>
            </li>
          </ul>
        </div>
      </nav>
      <div class="container mx-auto">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="Dashboard_Analyst_latest.php">Dashboard</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="stock-create.php">Stock Update</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Email</a>
                  </li>
            </ul>
        </nav>
      </div>
      <div class="container mx-auto">
        <h1 class="title">DASHBOARD</h1>
        <div class = "container-fluid container-primary bg-light p-5 mb-5">
            Notifications
        </div>

        <div class="dropdown">
            <button class="dropbtn">Filter By</button>
            <div class="dropdown-content">
              <a href="#">Stock ID</a>
              <a href="#">Product Name</a>
              <a href="#">Stock Quantity (Highest to Lowest)</a>
              <a href="#">Stock Quantity (Lowest to Highest</a>
              <a href="#">Price</a>
              <a href="#">Date Updated</a>
            </div>
          </div>

        <?php
          if (mysqli_num_rows($result) > 0) {
        ?> 
            <table class ="table">
                <thead>
                  <tr>
                      <th scope="col">Stock ID</th>
                      <th scope="col">Product Name</th>
                      <th scope="col">Price (RM)</th>
                      <th scope="col">Stock Quantity</th>
                      <th scope="col">Quantity Sold</th>
                      <th scope="col">Date</th>
                  </tr>
                </thead>
                <?php
            $i=0;
            while($row = mysqli_fetch_array($result)) {
            ?>
            <tr>
              <td><?php echo $row["stock_id"]; ?></td>
              <td><?php echo $row["product_name"]; ?></td>
              <td><?php echo $row["price_per_unit"]; ?></td>
              <td><?php echo $row["total_quantity"]; ?></td>
              <td><?php echo $row["quantity_sold"]; ?></td>
              <td><?php echo $row["date"]; ?></td>

              <td><a id ="update" href="stock-update.php? stock_id=<?php echo $row["stock_id"]; ?>">Update</a></td>
              <td><a id="delete" href="stock-delete.php? stock_id=<?php echo $row["stock_id"]; ?>">Delete</a></td>
            </tr>
              <?php
               $i++;
            }
              ?>


            </table>
            <?php
          }else{
            echo "No result";
          }
?>

<div class="container">
          <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
            <p class="col-md-4 mb-0 text-muted">&copy; Start Up Company, Inc</p>
        
            <a href="/" class="col-md-4 d-flex align-items-center justify-content-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
              <svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap"/></svg>
            </a>
          </footer>
        </div>
</body>
</html>