<?php

include 'config.php';

session_start();

$stock_id = null;  
$product_name = null;
$total_quantity = null;
$price_per_unit = null;
$quantity_sold = null;
$stock_status=null;
$date = date("m.d.y");
$role_name="Data Analyst";

$sql= "select stock_id from stock ORDER BY stock_id DESC LIMIT 1";
$result = mysqli_query($conn,$sql);
while($row = mysqli_fetch_array($result)) {
  $stock_id_last= $row['stock_id'] +1 ; 
}


if(isset($_POST['submit'])){
  $stock_id = $stock_id_last;
  $product_name = $_POST['product_name'];
  $total_quantity = $_POST['total_quantity'];
  $price_per_unit = $_POST['price_per_unit'];
  $quantity_sold = 0;
  $stock_status=1;
  $date = date("y-m-d");
  $role_name="Data Analyst";

  $sql = "INSERT INTO stock (stock_id, product_name, total_quantity, price_per_unit, quantity_sold, stock_status, date, role_name) 
          VALUES ($stock_id,'$product_name', $total_quantity, $price_per_unit, $quantity_sold, $stock_status, '$date', '$role_name')";
  
  $result = mysqli_query($conn, $sql);

  if ($result){
      echo "<script>alert('Stock created successfully!')</script>";

      $stock_id_last = $stock_id_last + 1;  
      $product_name = null;
      $total_quantity = null;
      $price_per_unit = null;
      $quantity_sold = null;
      $stock_status=null;
      $date1 = date("y-m-d");
      $role_name="Data Analyst";
      
      }else{
        echo "<script>alert('Woops! Something Wrong Went.')</script>";
      }
}

?>

<!doctype html>

<html lang="en">
  <head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
    <link rel="stylesheet" href="stock.css">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  
  </head>
  <body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#">Start-Up Inventory Management</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link" href="Dashboard_Analyst_latest.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="login.html">Log Out</a>
            </li>
          </ul>
        </div>
      </nav>
      <div class="container mx-auto">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="Dashboard_Analyst_latest.php">Dashboard</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="stock-create.php">Stock Update</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Email</a>
                  </li>
            </ul>
        </nav>
      </div>

    <form action="" method = "POST" class="form-group">
        <div class="form-group" >
          <h1>Create Stock</h1>
          <p>Please fill in the blanks</p>
          <hr>

          <label for="stock_id"><b>Stock ID:</b></label>
          <input type="number" placeholder="<?php echo $stock_id_last; ?>" name="stock_id" id="stock_id" min="<?php echo $stock_id_last; ?>" max = "<?php echo $stock_id_last; ?>">
          <hr>
      
          <label for="product_name"><b>Name of the Product:</b></label>
          <input type="text" name="product_name" placeholder="Name of the product" id="product_name" value="<?php echo $product_name;?>">
          <hr>
      
          <label for="total_quantity"><b>Quantity of product:</b></label>
          <input type="number" placeholder="Whole Number only" name="total_quantity" id="total_quantity" required value="<?php echo $total_quantity;?>" min=1>
          <hr>

          <label for="price_per_unit"><b>Price per unit:</b></label>
          <input type="number" step="0.01" placeholder="XX.XX" name="price_per_unit" id="price_per_unit" required value="<?php echo $price_per_unit;?>" min=0>
          <hr>
          
          <div class="btn-submit">
            <button type="submit" class="btn btn-primary" name="submit">Create</button>
          </div>
  
        </div>


        <div class="container">
          <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
            <p class="col-md-4 mb-0 text-muted">&copy; Start Up Company, Inc</p>
        
            <a href="/" class="col-md-4 d-flex align-items-center justify-content-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
              <svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap"/></svg>
            </a>
          </footer>
        </div>


      </form>

  </body>
</html>