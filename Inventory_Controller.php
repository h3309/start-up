<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Document</title>
</head>
<style>
    .navbar{
        margin-bottom: 70px;
    }
    .dropbtn {
  background-color: #000000;
  color: white;
  padding: 16px;
  padding-left: 20px;
  font-size: 16px;
  border: none;
}

.dropdown{
    float: right;
    position: relative;
}
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #ddd;}

.dropdown:hover .dropdown-content {display: block;}

.dropdown:hover .dropbtn {background-color: rgb(0, 153, 255);}
    ul.menu {
  list-style-type: none;
  margin-bottom: 50px;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

li.dash {
  float: left;
}

li.dash a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}
.download{
    margin-top: 20px;
    float: right;
    font-size: 20px;
    background-color: black;
    color: white;
    border-radius: 5px;
}
.download:hover{
    background-color: rgb(0, 153, 255);
}

footer {
  position:relative;
  bottom:0;
  width:100%;
  height:10%; 
  background-color: white;
  
}
</style>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#">Start-Up Inventory Management</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link" href="Inventory_Controller.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="login.html">Log Out</a>
            </li>
          </ul>
        </div>
      </nav>
      <div class="container mx-auto">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="Inventory_Controller.php">Dashboard</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="Dashboard_Controller_Report.php">Reports</a>
                </li>
            </ul>
        </nav>
      </div>
      <div class="container mx-auto">
        <h1 class="title">DASHBOARD</h1>
        <div class="dropdown">
            <button class="dropbtn">Filter By</button>
            <div class="dropdown-content">
              <a href="#">Stock ID</a>
              <a href="#">Product Name</a>
              <a href="#">Stock Quantity (Highest to Lowest)</a>
              <a href="#">Stock Quantity (Lowest to Highest</a>
              <a href="#">Price</a>
              <a href="#">Date Updated</a>
            </div>
          </div>
      <table class ="table">
          <thead>
            <tr>
                <th scope="col">Stock ID</th>
                <th scope="col">Product Name</th>
                <th scope="col">Price (RM)</th>
                <th scope="col">Stock Quantity</th>
                <th scope="col">Stock Sold</th>
                <th scope="col">Status</th>
                <th scope="col">Date Updated</th>
            </tr>
          </thead>
          <?php
          $db = mysqli_connect("localhost", "root", "", "start_up");
          if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
          }
          $sql = "SELECT stock_id, product_name, price_per_unit, total_quantity, quantity_sold, stock_status, date from stock";
          $result = $db -> query($sql);

          if ($result -> num_rows > 0){
            while ($row = $result -> fetch_assoc()){
              echo "<tr><td>" . $row["stock_id"] . "</td><td>" . $row["product_name"] . "</td><td>" . $row["price_per_unit"] . "</td><td>" .
               $row["total_quantity"] . "</td><td>" . $row["quantity_sold"] . "</td><td>" . $row["stock_status"] . "</td><td>" . $row["date"] . "</td></tr>";     
            }
            echo "</table>";
          }
          else {
              echo "0 result";
          }
          $db -> close();
          ?>
        
      </table>
      <button class="btn btn-primary">Download Table </button>
      <button type="button" class="btn btn-primary" onclick="window.location.href='Request Stock.php'">Request Stock</button></th>
    </div>

    <div class="container">
          <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
            <p class="col-md-4 mb-0 text-muted">&copy; Start Up Company, Inc</p>
        
            <a href="/" class="col-md-4 d-flex align-items-center justify-content-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
              <svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap"/></svg>
            </a>
          </footer>
        </div>
</body>
</html>
