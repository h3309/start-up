<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Document</title>
</head>
<style>
    .navbar{
        margin-bottom: 70px;
    }
    footer {
  position:relative;
  bottom:0;
  width:100%;
  height:10%; 
  background-color: white;
  
}
  
</style>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#">Start-Up Inventory Management</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link" href="Inventory_Controller.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="login.html">Log Out</a>
            </li>
          </ul>
        </div>
      </nav>
      <div class="container mx-auto">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="Inventory_Controller.php">Dashboard</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="Dashboard_Controller_Report.php">Reports</a>
                </li>
            </ul>
        </nav>
      </div>
      <div class="container mx-auto">
        <h1 class="title">REPORT MAINTENANCE</h1>
      <form>
        <div class="my-3">
          <label for="exampleName" class="form-label">Name</label>
          <input type="text" class="form-control" id="Name">
          <div id="emailHelp" class="form-text">Full Name</div>
        </div>
        <div class="my-3">
          <label for="exampleNum" class="form-label">Phone Number</label>
          <input type="tel" class="form-control" id="Number">
          <div id="numberHelp" class="form-text">Ex: 012-3456789</div>
        </div>
        <div class="my-3">
          <label for="exampleEmail" class="form-label">Email Address</label>
          <input type="email" class="form-control" id="Email">
          <div id="emailHelp" class="form-text">Use company registered email</div>
        </div>
        <div class="my-3">
          <label for="exampleName" class="form-label">Issue</label>
          <input type="text" class="form-control" id="Issue">
          <div id="emailHelp" class="form-text">Website Issue Faced</div>
        </div>
        <div class="my-3">
          <button type="button" class="btn btn-success" onClick="return Clear()" >Submit</button>
        </div>

      </form>
    </div>

    

    <div class="container">
          <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
            <p class="col-md-4 mb-0 text-muted">&copy; Start Up Company, Inc</p>
        
            <a href="/" class="col-md-4 d-flex align-items-center justify-content-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
              <svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap"/></svg>
            </a>
          </footer>
        </div>
</body>
</html>

<script>
function Clear()
{
  document.getElementById('Name').value = '';
  document.getElementById('Number').value = '';
  document.getElementById('Email').value = '';
  document.getElementById('Issue').value = ''

   return val;
}
</script>
