<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="register.css">

    <!-- font family -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">
</head>

<body>
    <div class="menubar">
        <div class="login">
            <span>Already registered? </span><a href="login.html" class="manu_btn btn-primary">Log in</a>
        </div>
    </div>

    <h2>Start-Up Inventory System</h2>

    <div class="wrapper">
        <div class="center">
            <h3>Create new account</h3>
            <form action="connection.php" method="post">

                <div class="field">
                    <input type="text" name="name" placeholder="Name / Business Name">
                    <br>
                </div>

                <div class="field">
                    <input type="number" name="phone_number" placeholder="Phone number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type="number" maxlength="11" required>
                    <br>
                </div>

                <div class="field">
                    <input type="text" name="email" placeholder="Email address">
                    <br>
                </div>

                <div class="field">
                    <input type="password" name="password" placeholder="Password" required>
                </div>


                <div class="field">
                    <label for="gender">Gender</label>
                    <label for="male" class="radio-inline"><input type="radio" name="gender" value="m" id="male" />Male</label>
                    <label for="female" class="radio-inline"><input type="radio" name="gender" value="f" id="female" />Female</label>
                    <label for="others" class="radio-inline"><input type="radio" name="gender" value="o" id="others" />Others</label> -->
                </div>

                <input type="submit" class="btn"><br>

                <span class="span" style="font-size: smaller;">By continuing, you acknowledge that you accept Start-Up's
                    <span style="color: blue;">Privacy Policies</span> and <span style="color: blue;">Terms &
                        Conditions</span>.</span>

            </form>

        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>

</html>